# ansible_pull role

Role to apply on hosts that should "self-update" using ansible-pull

It requires that the register-host playbook has been run, e.g. to add
the ansibleadmin user to the system.

## Variables

* `ansible_pull_repo`

    The git repository to clone/pull.

    This value must be set, it has no default value.

* `ansible_pull_inventory_relpath`


ansible_pull_repo: "git@gitlab.com:midgren-ansible/midgren-ansible.git"
ansible_pull_playbook: "playbooks/site.yml"
ansible_pull_extra_args: ""
ansible_pull_inventory_relpath: playbooks/inventory
